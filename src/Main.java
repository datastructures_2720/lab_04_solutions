import java.util.LinkedList;
import java.util.List;

import objects.DLine;

/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * -> Class: Data Structures - 2720 - - - - - - - - - - - - - - - - - - - - - -
 * -> LAB: 04 [Solutions] - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * -> Date: Friday 14 Sep, 2018 - - - - - - - - - - - - - - - - - - - - - - - -
 * -> Subject: Sorting  - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * -> Lab Web-page: - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *  [https://sites.google.com/view/azimahmadzadeh/teaching/data-structures-2720]
 *  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
 * @author Azim Ahmadzadeh [https://grid.cs.gsu.edu/~aahmadzadeh1/] - - - - - 
 */
public class Main {

	public static void main(String[] args) {

		SortMethods sm = new SortMethods();
		
		/* ------------ Warm-up 1 ------------ */ startSection(1);
		// TASK:
		// -Create an instance of DLine.
		// -Print the line using 'toString' method.
		// TODO
		DLine line1 = new DLine(12);
		System.out.println(line1.toString());
		/* ------------ End 1 ------------ */ endSection(1);
		//
		//
		//
		//
		/* ------------ Warm-up 2 ------------ */ startSection(2);
		// TASK:
		// -Create an list of DLine objects (use LinkedList)
		// -Add 5 different instances of DLine.
		// -Display the content of the list using 'displayList' in 'ISortMethods'.
		// TODO:
		List<DLine> myList = new LinkedList<>();
		myList.add(new DLine(12));
		myList.add(new DLine(9));
		myList.add(new DLine(6));
		myList.add(new DLine(3));
		myList.add(new DLine(1));
		sm.displayList(myList);
		/* ------------ End 2 ------------ */ endSection(2);
		//
		//
		//
		//
		/* ------------ Warm-up 3 ------------ */startSection(3);
		// TASK:
		// -Iterate over the list you created above
		// --- Compare each line to its previous line.
		// --- Print for each comparison: [first line], [second line], [comparison result]
		// TODO:
		for (int i = 0; i < myList.size() - 1; i++) {
			System.out.print("[" + myList.get(i) + "]");
			System.out.print(", [" + myList.get(i + 1) + "]");
			System.out.print(", [" + myList.get(i).compareTo(myList.get(i + 1)) + "]\n");
		}
		// -Q: What does the method 'compreTo' return?
		// TODO: ???
		//
		/* ------------ End 3 ------------ */ endSection(3);
		//
		//
		//
		//
		/* ------------ Warm-up 4 ------------ */startSection(4);
		// TASK:
		// -Store the first element of the list in 'tmpLine',
		// -Print myList.get(0),
		// -Change the size of the element in 'tmpLine',
		// -Again, print myList.get(0).
		// TODO:
		DLine tmp = new DLine(myList.get(0));
		System.out.println("First element [before]: " + myList.get(0).toString());
		tmp.length = 5;
		System.out.println("First element [after]: " + myList.get(0).toString());
		//
		// -Q: Anything strange? Why? How to resolve this issue?
		// TODO:
		/* ------------ End 4 ------------ */ endSection(4);
		//
		//
		//
		//
		/* ------------ Warm-up 5 ------------ */startSection(5);
		// TASK:
		// -Iterate over the list you created above
		// --- Shift each element one step back (toward left)
		// -Shoot the first element to the end.
		// -Display the content of the list, first and also after each change.
		// -Visually verify the results.
		// TODO:
		DLine tmpLine = new DLine(myList.get(0));
		sm.displayList(myList);
		for (int i = 1; i < myList.size() ; i++) {
			DLine thisLine = new DLine(myList.get(i));
			myList.set(i-1, thisLine);
			sm.displayList(myList);
		}
		myList.add(tmpLine);
		sm.displayList(myList);
		/* ------------ End 5 ------------ */ endSection(5);
		//
		//
		//
		//
		//
		/* * * * * * * * * * * * * * * * * * * * * * * 	*
		 * - - - - - - - - - - - - - - - - - - - - - - 	*
		 * - - - - - - CONGRATULATIONS - - - - - - - - 	*
		 * - - - - - - - - - - - - - - - - - - - - - - 	*
		 * You are now ready to start implementing the	*
		 * first method in class SortMethods.			*
		 * - - - - - - - - - - - - - - - - - - - - - - 	*
		 * - - - - - - - - - - - - - - - - - - - - - - 	*
		 */
		/* ------------ Sort I ------------ */startSection(6);
		// TASK:
		// -Create a new list of DLines as follows: {15,12,9,6,13,10,7,1}
		// -Use the sort method 'sort_1' to sort the list.
		// -Display your sorting steps by putting 'displayList' inside your sort method.
		// -Verify your algorithm by visually checking the final list.
		// TODO:
		// -Q: What sort algorithm do you think this is?
		// TODO
		/* ------------ End I ------------ */endSection(6);
	}

	
	
	
	
	/** IGNORE THIS METHOD **/
	private static void startSection(int i) {
		System.out.print("\n:::::::::::::::::::::::");
		System.out.print(" START [" + i + "] ");
		System.out.print(":::::::::::::::::::::::\n\n");
	}

	/** IGNORE THIS METHOD **/
	private static void endSection(int i) {
		System.out.print("\n________________________");
		System.out.print(" END [" + i + "] ");
		System.out.print("________________________\n\n");
	}

}
